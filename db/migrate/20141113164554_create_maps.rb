class CreateMaps < ActiveRecord::Migration
  def change
    create_table :maps do |t|
      t.text :map_info

      t.timestamps
    end
  end
end
