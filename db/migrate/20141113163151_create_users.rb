class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.integer :id_login
      t.string :name
      t.date :birthday
      t.string :locality
      t.string :photo

      t.timestamps
    end
  end
end
