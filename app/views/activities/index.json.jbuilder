json.array!(@activities) do |activity|
  json.extract! activity, :id, :name, :sport_id, :user_id, :map_id, :start_date, :duration, :distance
  json.url activity_url(activity, format: :json)
end
