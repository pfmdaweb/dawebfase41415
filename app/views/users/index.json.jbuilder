json.array!(@users) do |user|
  json.extract! user, :id, :id_login, :name, :birthday, :locality, :photo
  json.url user_url(user, format: :json)
end
