class ActivitiesController < ApplicationController
  before_action :set_activity, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_login!

  # GET /activities
  # GET /activities.json
  def index
    @activities = Activity.all
    if not params[:searchq].blank?
      @activities = @activities.where("name like ?", "%#{params[:searchq]}%")
    end
    @myActivities = Activity.where(user_id: current_login.user.id)
    if not params[:day].blank?
      @myActivities =@myActivities.where("start_date >= ?", params[:day])
    end
    if not params[:month].blank?
      @myActivities = @myActivities.where("strftime('%m', start_date) = ? AND strftime('%Y', start_date) = strftime('%Y', current_date)", params[:month])
    end
    if not params[:year].blank?
      @myActivities = @myActivities .where("strftime('%Y', start_date) = ?", params[:year])
    end
    @activities =  @activities.order('start_date DESC')
    @myActivities = @myActivities.order('start_date DESC')
  end

  # GET /activities/1
  # GET /activities/1.json
  def show
    @sports = Sport.all.collect { |s| [s.name, s.id]}
  end

  # GET /activities/new
  def new
    @sports = Sport.all.collect { |s| [s.name, s.id]}
    @activity = Activity.new
  end

  # GET /activities/1/edit
  def edit
    @sports = Sport.all.collect { |s| [s.name, s.id]}
  end

  # POST /activities
  # POST /activities.json
  def create
    @activity = Activity.new(activity_params)
    if params[:sport_name].blank?
      @activity.user_id = current_login.user.id
    else
      @sp = Sport.create(:name => :sport_name)
      @activity.user_id = current_login.user.id
      @activity.sport = @sp
    end
    respond_to do |format|
      if @activity.save
        format.html { redirect_to @activity, notice: 'Activity was successfully created.' }
        format.json { render :show, status: :created, location: @activity }
      else
        format.html { render :new }
        format.json { render json: @activity.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /activities/1
  # PATCH/PUT /activities/1.json
  def update
    respond_to do |format|
      if @activity.update(activity_params)
        format.html { redirect_to @activity, notice: 'Activity was successfully updated.' }
        format.json { render :show, status: :ok, location: @activity }
      else
        format.html { render :edit }
        format.json { render json: @activity.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /activities/1
  # DELETE /activities/1.json
  def destroy
    @activity.destroy
    respond_to do |format|
      format.html { redirect_to activities_url, notice: 'Activity was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_activity
      @activity = Activity.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def activity_params
      params.require(:activity).permit(:name, :sport_id, :user_id, :map_id, :start_date, :duration, :distance)
    end
end
