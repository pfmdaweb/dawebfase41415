class DashboardController < ApplicationController

  def index
    @friendships = Friendship.where(user_id: current_login.user.id, state: 'Confirmed')
    if not params[:searchq].blank?
      @myActivities = Activity.where("start_date like  ?", "%#{params[:searchq]}%")
      @myActivities = @myActivities.where(user_id: current_login.user.id).order('start_date DESC')
    end
    if not params[:id].blank?
      @friend_activities = Activity.where(user_id: params[:id]).order('start_date DESC')
    end
  end


end
