class FriendshipsController < ApplicationController
  before_action :set_friendship, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_login!

  # GET /friendships
  # GET /friendships.json
  def index
    @friendships = Friendship.where(user_id: current_login.user.id, state: 'Confirmed')
    @pendingFriendships = Friendship.where(user_id: current_login.user.id, state: 'Pending').order('created_at ASC')
    @receivedFriendships = Friendship.where(friend_id: current_login.user.id, state: 'Pending').order('created_at ASC')
  end

  # GET /friendships/1
  # GET /friendships/1.json
  def show

  end

  # GET /friendships/new
  def new
    @friendship = Friendship.new
  end

  # GET /friendships/1/edit
  def edit
  end

  # POST /friendships
  # POST /friendships.json
  def create
    @friendship = Friendship.create(friendship_params)
    respond_to do |format|
      if @friendship.save
        format.html { redirect_to @friendship, notice: 'Friendship was successfully created.' }
        format.json { render :show, status: :created, location: @friendship }
      else
        format.html { render :new }
        format.json { render json: @friendship.errors, status: :unprocessable_entity }
      end
    end
  end

  def invite
    Friendship.create(user_id: current_login.user.id, friend_id: params[:id], state: 'Pending')
    redirect_to friendships_path
  end

  def confirm
    Friendship.create(user_id: current_login.user.id, friend_id: params[:id], state: 'Confirmed')
    friend = Friendship.where(user_id: params[:id], friend_id: current_login.user.id).first
    friend.state = 'Confirmed'
    friend.save!
    redirect_to friendships_path
  end

  def delete
    @deleteFriend = Friendship.where(:user_id => current_login.user.id, :friend_id => params[:id_f])
    Friendship.delete(@deleteFriend)
    @deleteFriend = Friendship.where(:user_id => params[:id_f], :friend_id => current_login.user.id)
    Friendship.delete(@deleteFriend)
    redirect_to friendships_path
  end

  def cancel
    @deleteFriend = Friendship.where(:user_id => current_login.user.id, :friend_id => params[:id_f])
    Friendship.delete(@deleteFriend)
    redirect_to friendships_path
  end

  def reject
    @deleteFriend = Friendship.where(:user_id => params[:id_f], :friend_id => current_login.user.id)
    Friendship.delete(@deleteFriend)
    redirect_to friendships_path
  end

  # PATCH/PUT /friendships/1
  # PATCH/PUT /friendships/1.json
  def update
    respond_to do |format|
      if @friendship.update(friendship_params)
        format.html { redirect_to @friendship, notice: 'Friendship was successfully updated.' }
        format.json { render :show, status: :ok, location: @friendship }
      else
        format.html { render :edit }
        format.json { render json: @friendship.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /friendships/1
  # DELETE /friendships/1.json
  def destroy
    @friendship.destroy
    respond_to do |format|
      format.html { redirect_to friendships_url, notice: 'Friendship was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_friendship
      @friendship = Friendship.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def friendship_params
      params.require(:friendship).permit(:user_id, :friend_id, :state)
    end
end
