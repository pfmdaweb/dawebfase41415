class Activity < ActiveRecord::Base
  validates :sport_id, :map_id, :name, :duration, :distance, :start_date, presence: true
  belongs_to :sport
  belongs_to :user
  belongs_to :map
end
