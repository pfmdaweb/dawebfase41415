class Sport < ActiveRecord::Base
  validate :name, presence: true
  has_many :activities
end
