class Map < ActiveRecord::Base
  validates :map_info, presence: true
  serialize :map_info , JSON
  has_one :activity
end
